#include <iostream>
#include <fstream>

std::ifstream file;

int binary_search_with_left(int find, int* array, int left, int right)
{
    int r;
    while(left <= right)
    {
        r = (left + right) / 2;
        if(find > array[r])
        {
            left = r + 1;
        }
        else if(find < array[r])
        {
            right = r - 1;
        }
        else

            return r;

    }

    return -1;
}

void input_array(int* array, int n)
{
    for(int i = 0; i < n; i++)
        file >> array[i];
}

int main() {
    file.open("/home/alex/ClionProjects/tech_c_1_3_3/file.txt");

    int n = 0, m = 0;
    file >> n >> m;
    int *A = new int[n], *B = new int[m];
    input_array(A, n);
    input_array(B, m);

    int tmp_index;
    int left_split = 0;
    for(int i = 0; i < m; i++)
    {
        tmp_index = binary_search_with_left(B[i], A, left_split, n - 1);
        if(tmp_index >= 0)
        {
            std::cout << B[i] << " ";
            left_split = i + 1;
            if(left_split == n) break;
        }
    }

    file.close();

    return 0;
}