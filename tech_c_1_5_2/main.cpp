#include <iostream>
#include <assert.h>

class stack {
public:
    explicit stack(char buffer_size) : buffer_size(buffer_size), top(-1)
    {
        buffer = new char[buffer_size];
    };
    // TODO T
    stack(const stack& s)
    {
        buffer_size = s.buffer_size;
        top = s.top;

        buffer = new char[buffer_size];
        std::copy(s.buffer, s.buffer + buffer_size, buffer);
    }

    ~stack()
    {
        delete[] buffer;
    }

    void push(char el);
    char pop();

    int get_size() const { return top + 1; }
    bool is_empty() const { return top == -1; }

private:
    char *buffer;
    int buffer_size;
    int top;
};

void stack::push(char el) {
    assert(top < buffer_size - 1);
    buffer[++top] = el;
}

char stack::pop() {
    if(top == -1) throw -1;
    return buffer[top--];
}

bool f(const std::string out, const std::string target, unsigned int left, std::string now, stack* s) {
    //std::cout << out << " " << target << " -> " << now << " " << s->get_size() << "\n";

    if (now.length() == target.length()) {
        std::cout << target << " = " << now << " -> " << (now == target) << "; \n";
        return target == now;
    } else if (s->get_size() >= target.length())

        return false;

    stack tmp_stack = *s;
    bool add_in_stack = false;
    bool add_in_string = false;

    if (left < out.length()) {
        tmp_stack.push(out[left]);
        add_in_stack = f(out, target, left + 1, now, &tmp_stack);
    }

    if(tmp_stack.get_size() != 0) {
        std::string tmp_string = now;
        char el = tmp_stack.pop();
        //std::cout << "UP " << el << "\n";
        tmp_string.push_back(el);
        add_in_string = f(out, target, left + 1, tmp_string, &tmp_stack);
    }

    return add_in_stack || add_in_string;

}

int main() {
    stack stack_base(10);
    std::string in_string, out_string;
    std::cin >> in_string >> out_string;

    bool result = f(in_string, out_string, 0, "", &stack_base);

    std::cout << (result ? "YES" : "NO");

    return 0;
}