#include <iostream>

void input_array(int* array, const int n)
{
    for(int i = 0; i < n; ++i)
        std::cin >> array[i];
}

int main() {
    int k;

    int n; int* A;
    std::cin >> n;
    A = new int[n];
    input_array(A, n);

    int m; int* B;
    std::cin >> m;
    B = new int[m];
    input_array(B, m);

    std::cin >> k;

    bool change_a = true;
    int index_a = 0, index_b = m - 1;

    int result = 0;
    while(index_a < n && index_b >= 0)
    {
        if(change_a)
            A[index_a] = k - A[index_a];

        change_a = false;

        if(A[index_a] > B[index_b])
        {
            ++index_a;
            change_a = true;
        }
        else if(A[index_a] < B[index_b])

            --index_b;

        else
        {
            ++result;
            ++index_a; --index_b;
            change_a = true;
        }
    }

    std::cout << result;
    return 0;
}

/*
4
­5 0 3 18
5
­10 ­2 4 7 12
7
 */