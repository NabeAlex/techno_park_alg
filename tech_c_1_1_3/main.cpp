#include <iostream>
#include <assert.h>

class fraction
{
public:
    fraction(int a, int b)
    {
        this->numerator = a;
        this->denominator = b;
    }

    ~fraction() {}

    void reduce()
    {
        int min = this->gcd(this->numerator, this->denominator);
        this->numerator /= min;
        this->denominator /= min;
    }

    int get_numerator()
    {
        return this->numerator;
    }

    int get_denominator()
    {
        return this->denominator;
    }
private:
    int numerator, denominator;
    int gcd(int a, int b)
    {
        if(b == 0)

            return a;

        return gcd(b, a % b);
    }
};

fraction get_sum(fraction& a, fraction& b)
{
    assert(a.get_denominator() != 0 || b.get_denominator() != 0);

    int numerator = a.get_numerator() * b.get_denominator() +
                    b.get_numerator() * a.get_denominator();

    return fraction(numerator, a.get_denominator() * b.get_denominator());
}

int main() {
    int a, b, c, d;

    std::cin >> a >> b >> c >> d;

    fraction fraction_left = fraction(a, b);
    fraction fraction_right = fraction(c, d);

    fraction result = get_sum(fraction_left, fraction_right);
    result.reduce();

    std::cout << result.get_numerator() << " " << result.get_denominator();
    return 0;
}