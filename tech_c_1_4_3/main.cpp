#include <iostream>
#include <assert.h>

class stack {
public:
    explicit stack(int buffer_size) : buffer_size(buffer_size), top(-1)
    {
        buffer = new int[buffer_size];
    };

    ~stack()
    {
        delete[] buffer;
    }

    void push(int el);
    int pop();

    bool is_empty() const { return top == -1; }

private:
    int *buffer;
    int buffer_size;
    int top;
};

void stack::push(int el) {
    assert(top < buffer_size - 1);
    buffer[++top] = el;
}

int stack::pop() {
    if(top == -1) throw -1;
    return buffer[top--];
}

class stack_queue {
public:
    stack_queue(int buffer_size_in, int int_buffer_size_out)
    {
        in = new stack(buffer_size_in);
        out = new stack(int_buffer_size_out);
    }
    ~stack_queue()
    {
        delete in;
        delete out;
    }

    void queue(int el);
    int dequeue();

private:
    stack *in;
    stack *out;
};

void stack_queue::queue(int el) {
    this->in->push(el);
}

int stack_queue::dequeue() {
    if(this->out->is_empty())

        while(!this->in->is_empty())
            this->out->push(this->in->pop());

    return this->out->pop();
}

int main() {
    int n = 0, command, arg, tmp;
    std::cin >> n;
    stack_queue queue(n, n); // TODO n

    bool error = false;
    for(int i = 0; i < n; ++i)
    {
        std::cin >> command >> arg;
        switch(command)
        {
            case 2:
                try {
                    tmp = queue.dequeue();
                }
                catch(int e_int)
                {
                    tmp = e_int;
                }

                if(tmp != arg)
                {
                    error = true;
                }

                break;

            case 3:
                queue.queue(arg);
                break;

            default:
                break;
        }
    }

    std::cout << ((error) ? "NO" : "YES");

    return 0;
}